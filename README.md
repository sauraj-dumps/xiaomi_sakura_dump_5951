## cherish_sakura-userdebug 11 RQ3A.210905.001 eng.root.20210911.155703 release-keys
- Manufacturer: xiaomi
- Platform: msm8953
- Codename: sakura
- Brand: Xiaomi
- Flavor: cherish_sakura-userdebug
- Release Version: 11
- Id: RQ3A.210905.001
- Incremental: eng.root.20210911.155703
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Xiaomi/cherish_sakura/sakura:11/RQ3A.210905.001/root09111554:userdebug/release-keys
- OTA version: 
- Branch: cherish_sakura-userdebug-11-RQ3A.210905.001-eng.root.20210911.155703-release-keys
- Repo: xiaomi_sakura_dump_5951


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
